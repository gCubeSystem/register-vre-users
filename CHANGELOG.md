
# Changelog for Register VRE Users Portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.4.0]

- maven-portal-bom 4.0.0
- maven-parent 1.2.0

## [v2.3.0] - 2021-10-23

- Ported to git
- Removed home Library dep for sending messages

## [v2.2.0] - 2017-01-22

- Bug fix for Bug #6237, Users added to VRE when loggedin could not enter the VRE unless they logout first

## [v2.0.0] - 2016-06-08

- Ported to Liferay 6.2
- Changed visbility of users depending on the gateway
- Removed ASL Session
- Bug fix for Bug #6115 Register Users to VRE user cache problem

## [v1.4.2] - 2014-04-02

- Fix for Bug #1857 typo when displaying loading popup
- Anonymized usernames

## [v1.0.0] - 2014-04-02

First Release
